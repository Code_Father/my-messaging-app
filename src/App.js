import React from 'react';
import './App.css';
import Message from "./components/Message/message";
function App() {
  return (
    <div className="App">
      <Message/>
    </div>
  );
}

export default App;
